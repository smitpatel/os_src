typedef struct proc_prio {
    int pid;
    int prio;
} pp;

int compare(const void* d1, const void* d2) {
    pp* p1 = (pp*)d1;
    pp* p2 = (pp*)d2;
    if (p1->prio >= p2->prio) {
        return 1;
    } else return -1;
}

void print(pp* proc_list, int size) {
    pp* i_ptr = proc_list;
    int i = 0;
    printf("Processes in sorted order (priority)\n\n");
    while(i < size) {
        printf("PID %-5d Priority %-5d\n", i_ptr->pid, i_ptr->prio);
        i_ptr++;
        i++;
    }
}

void freePtr(void *ptr) {
    free(ptr);
    ptr = NULL;
}
