#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#define ACCURACY 6

typedef struct in_str {
    int x;
    int i;
} input;

void *sub_cal (void *i);
long double op;
long double accuracy = 1/pow(10, ACCURACY);

pthread_mutex_t e_x_mutex = PTHREAD_MUTEX_INITIALIZER;

int main() {
    int x, i = 0;
    printf("Enter x: ");
    scanf("%d", &x);

    for(;;i++) {
    	input in = { x, i };
    	pthread_t thr;
    	void *ret;
    	int p_ret = pthread_create(&thr, NULL, sub_cal, &in);
    	int th_ret = pthread_join(thr, &ret);
    	if (!ret) break;
    }
    printf("e^%d = %Lf\nTotal threads created %d\n", x, op, i);
}


void *sub_cal (void *in) {
    input *i = (input *)in;
    int f_op = 1, j = 1;
    long double ret;
    while(j <= i->i) {
    	f_op *= j++;
    }

    ret = pow(i->x, i->i)/f_op;
    pthread_mutex_lock(&e_x_mutex);
    op += ret;
    pthread_mutex_unlock(&e_x_mutex);
    if (ret < accuracy) return NULL;
    else return &j;
}
