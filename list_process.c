#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include "procc.h"

int main() {
    DIR *proc = opendir("/proc/");
    struct dirent *d;

    if (!proc) {
        printf("Error. Can't read process list");
        return -1;
    }

    int proc_count = count(proc);
    //pp** proc_list = (pp**) calloc(proc_count, sizeof(pp*));
    pp* proc_list = (pp*)calloc(proc_count, sizeof(pp));
    pp* m_ptr = proc_list;

    while((d = readdir(proc)) != (struct dirent *)0) {
        int pid;
        if ((pid = atoi(d->d_name)) > 0) {
            proc_list->pid = pid;
            proc_list->prio = getpriority(PRIO_PROCESS, pid);
            proc_list++;
        }
    }
    qsort(m_ptr, proc_count, sizeof(pp), compare);
    print(m_ptr, proc_count);
    freePtr(m_ptr);
    return 0;
}

int count(DIR* dir) {
    DIR* bak = dir;
    struct dirent *d;
    int size = 0;
    while((d = readdir(bak)) != (struct dirent *)0) {
        int temp;
        if ((temp = atoi(d->d_name)) > 0) {
            size++;
        }
    }
    rewinddir(dir);
    return size;
}
